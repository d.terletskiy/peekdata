﻿open Npgsql
open Dapper

open Giraffe
open Saturn
open FSharp.Control.Tasks.V2

module Query =
    let mostPopularSuppliers = """
        SELECT
            supp.companyname AS supplier_companyname,
            ord.shipcountry AS order_ship_country,
            SUM(1) AS order_productsalescount
        FROM public.orders AS ord
            JOIN public.order_details ord_det ON ord_det.orderid = ord.orderid
            JOIN public.products prd ON prd.productid = ord_det.productid
            JOIN public.suppliers supp ON supp.supplierid = prd.supplierid
        GROUP BY supplier_companyname, order_ship_country
        ORDER BY order_productsalescount DESC
    """

let execute (query: string) = task {
    use db = new NpgsqlConnection("Host=localhost;Port=5432;Database=northwind;Username=postgres;Password=docker;")
    return! query |> db.QueryAsync
}

let handle query next ctx = task {
    let! data = execute query
    return! json data next ctx
}

let router = router {
    get "/suppliers" (handle Query.mostPopularSuppliers)
}
    
let app = application {
    url ("http://127.0.0.1:5000/")
    use_router router
}

run app
